Source: python-cssselect
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: TANIGUCHI Takaki <takaki@debian.org>,
           Martin <debacle@debian.org>,
           Andrey Rakhmatullin <wrar@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-sphinxdoc <!nodoc>,
               python3-all,
               python3-doc <!nodoc>,
               python3-lxml <!nocheck>,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-sphinx <!nodoc>,
Standards-Version: 4.7.0
Homepage: https://github.com/scrapy/cssselect/
Vcs-Git: https://salsa.debian.org/python-team/packages/python-cssselect.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-cssselect
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no

Package: python3-cssselect
Architecture: all
Depends: python3, ${python3:Depends}, ${misc:Depends}
Description: cssselect parses CSS3 Selectors and translates them to XPath 1.0
 cssselect parses CSS3 Selectors and translate them to XPath 1.0 expressions.
 Such expressions can be used in lxml or another XPath engine to find the
 matching elements in an XML or HTML document.
 .
 This module used to live inside of lxml as lxml.cssselect before it was
 extracted as a stand-alone project.

Package: python-cssselect-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
Built-Using: ${sphinxdoc:Built-Using}
Suggests: python3-cssselect
Description: cssselect parses CSS3 Selectors and translates them to XPath 1.0 (documentation)
 cssselect parses CSS3 Selectors and translate them to XPath 1.0 expressions.
 Such expressions can be used in lxml or another XPath engine to find the
 matching elements in an XML or HTML document.
 .
 This module used to live inside of lxml as lxml.cssselect before it was
 extracted as a stand-alone project.
 .
 This package contains the documentation.
